let original = {
  name: "Aditya sharma",
  age: 22,
  language: {
    language1: "hindi",
    language2: "English", //#444
  },
};

let obj2 = original;
// obj2.name = "manish";

let obj3 = {...original};

// console.log(JSON.stringify(original));
// // {"name":"Aditya sharma","age":22,"language":{"language1":"hindi","language2":"English"}}
// let copy = JSON.parse(JSON.stringify(original));

// console.log(copy);
// console.log(copy.name); // deepClone of original
// console.log(copy.age);
// console.log(copy.language.language1);
// console.log(copy.language.language2);

// copy.name = "Sheetal sharma";
// copy.age=19;
// copy.language.language1="french";  //#444
// copy.language.language2="American-accent"; //#444

// console.log("original",original);
// console.log("copy",copy);
