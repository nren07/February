// let a = 100;
// let b = a;

// console.log(a);
// console.log(b);

// b = 60;

// console.log(a);
// console.log(b);

// 1st method of cloning is '=' (assignment operator) -> shallow clone
// let obj1 = {
//   a: 10,
// };

// let obj2 = obj1;

// console.log(obj1);
// console.log(obj2);

// obj2.a = 70;

// console.log(obj1);
// console.log(obj2);

// 2nd method of cloning is '...' (spread operator) -> shallow clone

// // ex - 1 for 2nd method
// let obj1 = {
//   a: 10,
//   b: 20,
// };

// let obj2 = { ...obj1 };

// console.log('obj1 ->',obj1);
// console.log('obj2 ->',obj2);

// obj2.a = 90;
// obj2.b = 100;

// console.log('obj1 ->',obj1);
// console.log('obj2 ->',obj2);

// // exp - 2 for 2nd method

// let obj1 = {
//   a: 10,
//   b: 20,
//   c: {
//     d: 90,
//   },
// };

// let obj2 = { ...obj1 };

// console.log('obj1 ->',obj1);
// console.log('obj2 ->',obj2);

// obj2.a = 90;
// obj2.b = 100;
// obj2.c.d = 200;

// console.log('obj1 ->',obj1);
// console.log('obj2 ->',obj2);


// 3rd method is JSON.stringify & JSON.parse -> deepClone
// let obj1 = {
//   a: 10,
//   b: 20,
//   c: {
//     d: 90,
//   },
// };

// const str = JSON.stringify(obj1);
// // console.log(obj1);
// // console.log(str);
// // {"a":10,"b":20,"c":{"d":90}}

// const obj2 = JSON.parse(str);
// console.log(obj1);
// console.log(obj2);

// obj2.a=100;
// obj2.b=200;
// obj2.c.d = 900;

// console.log(obj1);
// console.log(obj2);


