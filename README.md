
## Instructor

- [@Linkedin](https://www.linkedin.com/in/prikshit8/)
- +91-8800857472

## Schedule

<table>
  <thead>
    <tr>
      <th>Lecture No.</th>
      <th>Date</th>
      <th>Day</th>
      <th>Topic</th>
      <th>Notes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>1</td>
      <td>7<sup>th</sup> Feb</td>
      <td>Wednesday</td>
      <td><ul><a href="https://gist.github.com/prikshit8/f469566371015353e57511989cd07b94"><li>Object cloning</li></a><li>deep vs shallow</li></ul></td>
      <td><a href="https://drive.google.com/file/d/1fVNsy38rhpkqerr8Yi3jtGpUT686GbM4/view?usp=sharing">Notes</a></td> 
    </tr>
    <tr>
      <td>2</td>
      <td>8<sup>th</sup> Feb</td>
      <td>Thursday</td>
      <td><ul><a href="https://gist.github.com/prikshit8/87d13ce19b1ea4d1aa1f31c912def513"><li>My Structured Clone</li></a><a href="https://gist.github.com/prikshit8/7c9962b1814f14ba748542d261a483df"><li>Call</li></a><li>Apply</li><li>Bind</li></ul></td>
      <td><ul><a href="https://drive.google.com/file/d/1iay7sXzXe7i3_48_rj7VyQxqjswcLwXE/view?usp=sharing"><li>My Structured Clone Notes</li></a><a href="https://drive.google.com/file/d/1XE7IHpxUVY9lqIXYtiKdV-yDo1JyuynW/view?usp=sharing"><li>call notes</li></a></ul></td> 
    </tr>
    <tr>
      <td>3</td>
      <td>9<sup>th</sup> Feb</td>
      <td>Friday</td>
      <td><ul><li>Prototypes</li><li>Prototypical Inheritance</li></ul></td>
      <td><a href="https://drive.google.com/file/d/1FagwrNMVE7euyXapeRLVrexUfmjilrY5/view?usp=sharing"><li>PROTOTYPE NOTES</li></a></td>
    </tr>
    <tr>
      <td>4</td>
      <td>12<sup>th</sup> Feb</td>
      <td>Monday</td>
      <td><ul><li>Function to find depth of an array</li><li>Object Problems</li><li>Depth controlled flat</li><li>For in loop limitations</li><li>Flatten the arrray - 1</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>5</td>
      <td>13<sup>th</sup> Feb</td>
      <td>Tuesday</td>
      <td><ul><li>Map polyfill</li><li>Filter Polyfill</li><li>Reduce Polyfill</li><li>Call polyfill</li></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>6</td>
      <td>14<sup>th</sup> Feb</td>
      <td>Wednesday</td>
      <td><ul><li>Apply Polyfill</li><li>Bind Polyfill</li><li>OOPS Concepts</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>7</td>
      <td>15<sup>th</sup> Feb</td>
      <td>Thursday</td>
      <td><ul><li>OOPS CONTINUED</li><li>OOPS Questions</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>8</td>
      <td>16<sup>th</sup> Feb</td>
      <td>Friday</td>
      <td><ul><li>Callback</li><li>Callback hell</li><li>Promises</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>9</td>
      <td>19<sup>th</sup> Feb</td>
      <td>Monday</td>
      <td><ul><li>Promise questions</li><li>Promise combinations</li><li>Promise polyfills</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>10</td>
      <td>20<sup>th</sup> Feb</td>
      <td>Tuesday</td>
      <td><ul><li>Async await</li><li>Fetch</li><li>forEach limitations with async await</li><li>APIs</li><li>LocalStorage, Cookies & SessionStorage</li><li>Questions</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>11</td>
      <td>21<sup>st</sup> Feb</td>
      <td>Wednesday</td>
      <td><ul><li>Express Server</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>12</td>
      <td>22<sup>nd</sup> Feb</td>
      <td>Thurday</td>
      <td><ul><li>Chat App</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>13</td>
      <td>23<sup>rd</sup> Feb</td>
      <td>Friday</td>
      <td><ul><li>Debouncing</li><li>Throttling</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>14</td>
      <td>26<sup>th</sup> Feb</td>
      <td>Monday</td>
      <td><ul><li>YouTube Clone</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>15</td>
      <td>27<sup>th</sup> Feb</td>
      <td>Tuesday</td>
      <td><ul><li>Event Delegations</li><li>Questions</li></ul></td>
      <td>Notes</td>
    </tr>
    <tr>
      <td>16</td>
      <td>28<sup>th</sup> Feb</td>
      <td>Wednesday</td>
      <td><ul><li>Advanced questions</li></ul></td>
      <td>Notes</td>
    </tr>
      <tr>
      <td>17</td>
      <td>29<sup>th</sup> Feb</td>
      <td>Tuesday</td>
      <td><ul><li>Advanced questions</li></ul></td>
      <td>Notes</td>
    </tr>
  </tbody>
</table>