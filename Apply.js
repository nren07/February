var nam = 'rahul';

let obj = {
  nam: 'prikshit',
};

// my intro function is binded with window obj
function intro(workplace,city) {
  console.log("my name is " + this.nam + " I work at " + workplace + " I live in " + city);
}

intro(); // intro binded with window with no functional arguments
intro.apply(obj);
intro.call(obj);

// there's no difference if you are 
// calling a fn using call & apply
// method without functional arguments

console.log('---------');
intro('amazon','gurgaon');
intro.call(obj, 'walmart','bengaluru');
intro.apply(obj,['uber','bengaluru']);

// in apply we need to pass the arguments
// in the form of array

// in case of call -> obj context, arg1, arg2 , arg3.........
// in case of apply-> obj context, [arg1,arg2,arg3.......]

// call can accept multiple/infinite arguments
// apply can accept only 2 arguments 
