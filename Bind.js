var nam = "rahul";

let obj = {
  nam: "prikshit",
};

// my intro function is binded with window obj
function intro(city, workplace, age) {
  console.log("my name is " + this.nam + " I work at " + workplace + " I live in " + city + " my age is " + age);
}

// intro.call(obj,'walmart','bengaluru');
// intro.call(obj,'cars24','gurgaon');
// intro.call(obj,'uber','hyderabad');

// call & apply were immediately invoking the function with the given
// this keyword value which is basically an obj

// bind says I will give you a new function in which
// this keyword is permanently binded

const newIntro = intro.bind(obj); // here this keyword was fixed

// the value of this keyword inside newIntro will be obj
// and newIntro function will have defi same as intro
// newIntro('bengaluru','walmart'); // here this is obj
// newIntro('hyderabad','uber');
// newIntro('gurgaon','cars24');

const introCityFixed = intro.bind(obj,'delhi','walmart',24,'any random stuff');
introCityFixed(); 
introCityFixed('bengaluru');

// now the problem is I want to fix the city
// as well


// you can fix arguments using bind
// but the order will be left to right

// you cannot fix any random


// newIntro & intro they both are same
// the value of intro is dependent on how you call it
// but the value of this keyword inside newIntro is fixed
// that is the object you passed in bind
