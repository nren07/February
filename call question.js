var age = 10;
var name = 'rahul';

var person = {
  name: "prikshit",
  age: 24,
  getAge: function () {
    return this.age;
  },
};

var person2 = {
  age: 28,
  name:"aditya"
};

console.log(person.getAge()); // here this is person
console.log(person.getAge.call(person)); // here this is person
console.log(person.getAge.call(person2)); // here this is person2
console.log(person.getAge.call(window)); // here this is window
