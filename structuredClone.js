// https://developer.mozilla.org/en-US/docs/Web/API/structuredClone

// write your own structured clone
// polyfill of structed Clone

const deepClone = (obj) => {
  // are we sure if this obj is an obj??
  const type = typeof obj;
  if (type !== "object" || obj === null) return obj; // it will act as assignment

//   obj can be array as well
// [1,{},'hello'] // this is an actual array
// this is algo for arrays
if(Array.isArray(obj)){
    // // [1,{},'hello']
    // [deepClone(1),deepClone({}),deepClone('hello')];
    return obj.map(value => deepClone(value));
}

// algorithm for objects
// [[index0,1],[index1,{}],[index2,'hello']] // array version of obj

  // so from here I am damn sure it's an object

  //  now the looping part
  //   convert my obj into an array
  const arrObj = Object.entries(obj);
  //   using this arrObj. I have to make
  //   a deepClone version of arrObj
  const deepCloneArr = arrObj.map((item) => [item[0], deepClone(item[1])]);

  return Object.fromEntries(deepCloneArr); // Object.fromEntries is opposite of Object.entries
};

const object1 = {
  a: "somestring", //['a', 'somestring']
  b: 42,
  c: {
    d: 100,
  },
};

const obj2 = deepClone(object1);

console.log(object1);
console.log(obj2);

obj2.a ='hello';
obj2.c.d = 200;

console.log(object1);
console.log(obj2);